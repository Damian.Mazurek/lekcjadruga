package tasks;

import java.util.Scanner;

public class ZadaniePiate {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Podaj imię z małej litery: ");
        String name = in.next();
        String firstLetter = name.substring(0, 1);
        String restOfName = name.substring(1);

        System.out.println(firstLetter.toUpperCase() + restOfName);

    }
}
