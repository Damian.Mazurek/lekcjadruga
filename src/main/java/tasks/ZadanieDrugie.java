package tasks;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ZadanieDrugie {
    public static void main(String[] args) {
        try {
            System.out.println
                    ("Podaj liczbę aby sprawdzić czy jest podzielna przez 3: ");

        Scanner sc = new Scanner(System.in);
        int liczba = sc.nextInt();

        if (liczba % 3 == 0) {
            System.out.println("Liczba jest podzielna przez 3");
        } else {
            System.out.println("Liczba nie jest podzielna przez 3");
        }
    }catch (InputMismatchException exception){
            System.out.println("Błąd podczas wpisywania liczby");
        }
    }
}
